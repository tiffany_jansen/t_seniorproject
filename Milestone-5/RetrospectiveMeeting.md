Class Project Retrospective Meeting Notes
=========================================

1. Recite the prime directive of retrospectives
    * "Regardless of what we discover, we understand and truly believe that everyone did the best job they could, given what they knew at the time, their skills and abilities, the resources available, and the situation at hand." --Norm Kerth, Project Retrospectives: A Handbook for Team Review
2. Conduct a Safety Check
    * 3.5
    * ![Image](SafetyCheck.png)
3. What did you learn from the Sprint?
    * They are hard.
    * Communication is KEY
        * We need more face-to-face discussion time.
            * More collaborative than in the past.
        * Coordination
        * More Voice Chats and Screen Sharing
    * We are good at separating tasks, but not so good at making sure everyone does the “same” amount.
    * Research is very important.
        * Google is our friend!
    * Good to get started early in case we run into issues near the end.
4.  What still isn't going right?
    * Scheduling Conflicts
        * Not enough face-to-face meetings
    * Not ‘completely’ comfortable talking about issues.
    * Occasional problems with git/bitbucket.
        * Including but not limited to differences in versions or the project working for some people but not everyone.
5.  What can the team do better during the next Sprint? Make a specific Action Plan.
    * Do more face-to-face meetings.
        * Wednesday: after 3 everyone is available to meet face-to-face
        * Tuesday: after 6 probably on voice chat/screen sharing
        * Monday: 12 - 1 get as much stuff done as possible.
    * Get things done earlier so we can fix errors/bugs before the due date.
    * Make sure we check out backlog items before we start and during the sprint.
6.  Are there any items that need to be brought up with someone outside the team? (i.e. in this case the instructor)
    * Nope.
 
