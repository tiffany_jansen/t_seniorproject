﻿2018-19 Class Project Inception: Discussion Hub
===============================================

## Vision Statement
For users who are interested in discussing and/or challenging different media topics in an open conversational environment.  The “Discussion Hub Website” is a site dedicated to providing an open, yet civilized discussion forum where users can review and discuss news and other media topics.  The site will allow users to provide counter arguments, facts, opinions, or just general concerns on a variety of media topics originating from multiple sources. The site is designed to maintain anonymity, but will also include a reputational component. As users create discussions and comments, other users will be allowed to vote to have these placed within specified categories allowing for better overall organization within each discussion, at the same time create more constructive and meaningful conversations. Unlike other open discussion forums, toxicity and trolling will be highly discouraged and weeded out. A reputation page will be created for each user that allows for peer to peer review of how each user is contributing to the overall community and allow for admin intervention in the case of severe abuse. This reputation will be adjusted based on how each discussion the user posts was categorized as well as up/down votes on their posted content. A user will be able to review their personal reputation at any time, and can adjust their behavior within the site as necessary if they are not seeing the reputation that they would like. By having a peer to peer reviewed discussion forum, users will feel that their opinions are more easily expressed and heard.  All the while creating a more respectable, civilized, and welcomed discussion location.

## Summary of Our Approach to Software Development
We will be following the agile principles as described in the Disciplined agile Delivery textbook. As well as following the scrum project management iterative process. 

## Initial Vision Discussion with Stakeholders
People like to talk; people like to discuss and argue. People like to state their opinions and read thoughts of others. But there are real problems with how it is happening on the Web. Just take a look at YouTube or Twitter comments. That's not a discussion and it's not often useful, productive or even civil. Social media is not the place to talk about things. The Internet should be a place where people can communicate. Let's make a site where people can do that!
Places like Reddit and Kialo already have a handle on general topic discussions, so we won't try to do that. Enthusiasts often have their own sites with discussion and comment sections (e.g. Slashdot) and have formed close-knit communities that work well. We don't want to do that either. Here's our idea, using an example:
Let's say I just read this article on CNN, American endurance athlete becomes the first person to cross Antarctica solo, about a guy from Oregon who skied across Antarctica. The thing is, is that he didn't and he wasn't the first. His claims should be challenged and the article/journalist is disingenuous by not at least bringing this up. People reading this article should know there are serious questions about it. Someone should comment on this to point it out and spark further discussion. There is no way to do this. CNN does not have a comment section. Even if it did, do I want to create an account there just so I can make a quick comment? I'd have to do that everywhere on the web where I had a question or wanted to comment. How would I know if my question was answered?
We want a centralized discussion site that can be found easily and where an individual can maintain an account, build a history, expertise, level of trust, etc. It should make it easy to create or find a discussion page about any news article, post or web site. It should provide features for the user to follow their discussions without ever going back to the original website. It should allow them to create and maintain their own identity that is separate from any social media identity.

## Initial Requirements Elaboration and Elicitation

**Questions**

1. How do we link a discussion on the site to one or more articles/pages?
      * We were thinking via URL and title of article.
2. How will users find out that there is a discussion on the site for the article/page they're currently viewing
      * How about a browser plug-in? It could send the URL of the current page to our API to see if a discussion page exists and provide an easy way for them to navigate to our page.
      * Or the user can copy the URL (or article title) and paste it into a search bar on our site.
3. Clearly we need accounts and logins. Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or …?
      * We would have our own account creation and user history information.
4. Do we allow people to comment anonymously? Read anonymously?
      * You can not comment anonymously, but you may read anonymously.
5. Do we allow people to sign up with a pseudonym or will we demand/enforce real names?
      * Pseudonyms will be allowed.
6. What is it important to know about our users? What data should we collect?
      * User report will be included -- based on feedback from other users: 
      * Beyond just “upvote” and “downvote” -- also include specific tags for comments such as “useful”, “not useful”, “trolling”, and “rude” etc
      * Includes how users rate others maybe? 
7.  If there are news articles on multiple sites that are about the same topic should we have separate discussion pages or just one?
      * Multiple news sources allowed per topic -- each story will have it’s own unique tag that links all stories on the same topic together.
      * The tag page also has a discussion section for all articles together.
8.  What kind of discussion do we want to create? Linear traditional, chronological, ranked, or ?
      * We can set a default of chronological, but allow users to change filter/sort depending on how they want it sorted. At a specific(?) threshold we can change the sort to get the better ranked ones above.
9.  Should we allow image/video uploads and host them ourselves?
      * No. This is more discussion base and should use links if you want to share something like that.
10. How do we want to deal with extremely large discussion topics that generate an excessively large amount of discussion.
      * We could consider having multiple discussion categories for each Article page, examples like Facts, Counter Facts, General, Opinion Based, Complaint Area, etc…
11. How do we manage inappropriate names/content to maintain a civil environment for users?
      * We can use name filtering and users will be able to report inappropriate names and/or comments and once certain thresholds are met, admins would be notified to investigate and/or edit as needed. 
12. How would you prevent banned users from constantly making new accounts to re-enter?
      * We can ban the use of the same email for an account that has been banned.
13. Should we include a virus checker for links that have been uploaded as comments?
      * Yes. We can find an API/PlugIn that will allow us to make sure that it isn’t a virus.

**Interviews**
We have decided as a team that we do not need to do interviews since we were very thorough with our questions above.  In addition we feel that our user stories worked well to address the needs and requests of our stakeholders.

## List of Needs and Features
1. A great looking landing page with info to tell the user what our site is all about and how to use it. Include a link to and a page with more info. Needs a page describing our company and our philosophy.
2. The ability to create a new discussion page about a given article/URL. This discussion page needs to allow users to write comments.
3. The ability to find a discussion page.
4. User accounts.
5. A user needs to be able to keep track of things they've commented on and easily go back to those discussion pages. If someone rates or responds to their comment we need to alert them.
6. Allow users to identify fundamental questions and potential answers about the topic under discussion. Users can then vote on answers.
7. Find an API/PlugIn that will be able to check for viruses when users post links as their comment.
8. The ability to filter the comments (being able to sort/filter them) on a discussion page.
9. The ability for a user to add a tag to an article that they have submitted for discussion.
10. The ability for users to vote as to if an discussion topic is useful, factual, trolling, inappropriate,etc...and to make sure that x amount of users choose this section before the discussion is moved to its appropriate location as a result.
11. A profile page for each user that includes totals for their discussions, and their overall reputation on the site as people up, down vote, call them out for trolling etc...This could work as a landing page that links to feature 5 above.
12. A favorites page that allows users to track discussions on their favorite topics so that they can quickly go to anything they want to start commenting on or discuss.
13. The ability to look through different discussion categories for an article(in the event that the discussion topic is large enough to warrant having discussion categories.)
14. A sign up page to make an account using an email not already in use
15. Social media icons to share discussion pages.

## Initial Modeling

**Use Case Diagram**
![Image](UseCaseDiagram.jpg)

**Other Modeling**
_Admin_ -> can delete/alter discussion pages -> remove comments -> tag comments/articles -> ban accounts -> move comments
_Moderator_ -> remove comments -> report users/comments -> move comments
_User (Logged In)_ -> signs up for website -> can post news articles -> can comment -> can upvote/downvote/etc  -> can tag comments/articles -> report users/comments -> search discussions -> can add a new tag to the list of tags
_Guest_ -> can search discussions ->can view discussions/comments

_(roles scale upward -- user can do everything guest can, moderator everything user can...etc)_

## Identify Non-Functional Requirements
1. User accounts and data must be stored indefinitely.
2. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
3. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
4. Must work in all languages and countries. English will be the default language but users can comment in their own language and we may translate it.
5. A user agreement needs to be created which is agreed to by each user during their account creation which allows us to ban inappropriate users, and/or send warnings to those that begin to abuse the site by trolling, inappropriate or racial content, etc...as their profile updates.
6. A contact us section which may include Site administrators who will be in charge of looking into potential abuse or concerns from other users. 
7. Site should be able to handle at least 500000 users at a time without crashing.  
8. Site should automatically send a confirmation email once a user has signed up.
9. Site should automatically make sure that the user has confirmed said email before allowing them to comment on any discussion.
10. Make sure that thresholds are set for all of the Admin duties so each Admin is notified when the thresholds are reached so the Admin can perform the required actions necessary, i.e. Banning inappropriate users, moderating/removing inappropriate comments, etc.
11. Add a .gitignore to make sure we don’t end up with a bunch of extra stuff in our Repo.

## Identify Functional Requirements (User Stories)

**E: Epic**
**F: Feature**
**U: User Story**
**T: Task**

1. [U] As a guest to the site I would like to see a fantastic and modern homepage that tells me how to use the site so I can decide if I want to use this service in the future.
      * [T] Create starter ASP dot NET MVC 5 Web Application with Individual User Accounts and no unit test project.
      * [T] Switch it over to Bootstrap 4
      * [T] Create nice homepage: write content, customize navbar.
      * [T] Create SQL Server database on Azure and configure web app to use it. Hide credentials.
2. [F] Fully enable Individual User Accounts.
      * [T] Copy SQL schema from an existing ASP.NET Identity database and integrate it into our UP script.
      * [T] Configure web app to use our db with Identity tables in it.
      * [T] Create a user table and customize user pages to display additional data.
3. [F] Allow user to create new discussion page.
4. [F] Allow guest to search for and find an existing discussion page.
5. [E] Allow a user to write a comment on an article in an existing discussion page
6. [U] As a logged in user, I would like to be able to vote to move a discussion topic under a specific category (Useful, Not-Useful, Rude, etc...) so that the discussions and responses stay within their respective locations.
      * [T] Create category names for each type.
      * [T] Each discussion should have a tab created for each category.
      * [T] Set a Vote Amount to X amount of votes before it is moved to a specific tab.
7. [U] As a user, I would like to be able to flag a comment or discussion topic as inappropriate so that after so many votes the post can be reviewed and/or removed.
      * [T] Create each tag for comments -- useful, not useful, rude, troll, good
8. [U] As a logged user, I want to be able to add a discussion to my Favorites so I can access it more easily.
      * [T] Add the Favorites Page to the user accounts.
9.  [E] Include a page for each user that displays their profile and stats.
    * [U] As a logged in user, I want to be able to view my or another user’s profile page and stats.
      * [T] Create a profile landing page for each user with basic info.
      * [T] Profile page includes a user report showing their comment and discussion statistics, history, and a pie chart showing how their comments have been flagged by other users.
10. [U] As a user, I want to be able to report users or comments to an moderator/admin for inappropriate behavior.
    * [T] Add a report option to comments/user profile pages that send reports directly to moderators/admins.
    * [T] A report allows reporting user to provide comments or a drop down menu selection explaining why they are reporting said comment/user.
11. [U] As a moderator I want to be able to remove comments that are inappropriate so other users don’t feel attacked or trolled.
    * [T] Set up a Moderator tag so we know who is a moderator.
    * [T] Add a remove comment option.
12. [U] As an admin I want to be able to ban users that are being inappropriate so other users can have a decent time on our site.
13. [U] As a user I want to be able to create a new tag so I can tag a discussion with my new tag.
14. [U] As a moderator I want to be able to report users to the admin so I can help people feel more comfortable on the site.
15. [U] As a user I would like a share icon on discussion pages so that I can share interesting things with friends over social media
    * [T] Create social media icons (custom or some default one)
    * [T] Setup API to be able to use icons
16.  [U] (Hector) As a visitor I would like a sign-up page so that I can make an account to post comments on discussion pages
    * [T] create a sign-up page
    * [T] require certain amount of personal info to make account
17. [U] As a robot I would like to be prevented from creating an account on your website so I don't ask millions of my friends to join your website and add comments about male enhancement drugs.

## Initial Architecture Envisioning
* SQL Server (Database Storage)
* Azure for deployment
* ASP.NET/Visual Studio
* Client/Server Functionality
* Network
* MVC is going to be our software design
* API’s to be used.
  * API to detect potential virus links
  * API to share/link to social media accounts
  * Recapta API for checking for Robots

## Agile Data Modeling

![Image](ERD.jpg)

## Timeline and Release Plan
This section is unnecessary since we will not actually be completing this project.