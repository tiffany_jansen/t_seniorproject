# Coding Guidelines

Please follow the guidelines listed below when contributing code to any projects within this repository.


## General Rules

* We only want to be writing code for this Poject in C#, HTML, and RAZOR
* SQL Scripts and Database information should be consistent.  We are always specific with each field in the database, for example ID for a table should be specific to that table i.e. UserID
* All code should be commented where approaprite:
    * C# --> XML commenting styles
        * /// <summary>
          /// 
          /// </summary>
    * HTML --> RAZOR commenting styles
        * @**@
    * Javascript --> Specific to the language
        * //

## C# Style

* Put curly braces on their own line:
(exception: basic {get;set;})

```cs

public class AA
{
    public int ID { get; set; }
}
    
```

## External Files in C#

* All Javascript (.js) and css (.css) should be located inside seperate files and should be named approapritely.

## C# Naming Conventions

* __**Object Name: Notation**__

* **Class name:** PascalCase
* **Constructor name:** PascalCase
* **Method name:** PascalCase
* **Method arguments:** camelCase
* **Local variables:** camelCase
* **Constants name:** PascalCase
* **Field name:** camelCase
* **Properties name:** PascalCase
* **Delegate name:** PascalCase
* **Enum type name:** PascalCase

## Git

* Use branches
* Commit often
* Write good commit messages
* All feature branches should have the following format: 
    *ft
    *_feature#
    *_developerName
    * **Example:** (ft_46_John)
* Make sure that your feature branch # matches the feature you are adding from Dev-Ops
* Don't commit code that doesn't compile
* Please notify the Repo owner of Pull Requests