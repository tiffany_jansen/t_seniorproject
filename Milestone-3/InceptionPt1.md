Group Project Inception Phase Part 1
=====================================

##Initial Vision

A one-stop shop for travel for those without a good concept of cost and need assistance determining where and how they can travel under a specific budget. It would allow users to find an estimate of how much it would cost to travel to a specific country and eventually certain popular destinations. Our starting point will be to focus on flights and lodging with additional concepts down the line such as: food, transportation, events, weather, crime in the area, etc. 

##Initial Requirements

**Web Pages**

* Homepage
    * User Accounts - _Optional_
        * To save search history/save destinations
    * Header
        * Title
        * "Sweet" Logo
    * Search Bar
        * Destinations
    * Form for Information
        * Budget
        * Number of People
        * Children v Adults
        * Dates of Travel/Season
        * Destination?
* Personal Travel Wishlist Page - _Optional_
    * For logged-in users
* Search Results Landing Page
    * Destinations
    * The info they put
    * Weather
    * Pictures
    * Crime Rate
    * Other Info
* Create An Account Page - _Optional_
* Popular Destinations
    * Pictures of Popular Places
        * Average cost for 1 person -lodging
        * Links to enter info to see how much it would be to travel to the place.

##Mind-Map
![Image](mindMap.png)