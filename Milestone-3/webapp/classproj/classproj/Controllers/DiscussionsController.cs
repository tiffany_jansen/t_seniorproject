﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using classproj.Models;

namespace classproj.Controllers
{
    public class DiscussionsController : Controller
    {
        private DiscussionDB db = new DiscussionDB();

        // GET: Discussions
        public ActionResult Index(string sortOrder)
        {
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email_desc" : "";
            ViewBag.ArticleSortParm = String.IsNullOrEmpty(sortOrder) ? "article_desc" : "";
            ViewBag.DiscussionSortParm = String.IsNullOrEmpty(sortOrder) ? "discussion_desc" : "";
            ViewBag.SubmissionSortParm = String.IsNullOrEmpty(sortOrder) ? "submision_desc" : "";
            var discussions = from d in db.Discussions
                              select d;
            switch (sortOrder)
            {
                case "email_desc":
                    discussions = discussions.OrderBy(d => d.CreatorID);
                    break;
                case "article_desc":
                    discussions = discussions.OrderBy(d => d.ArticleTitle);
                    break;
                case "discussion_desc":
                    discussions = discussions.OrderBy(d => d.DiscussionTitle);
                    break;
                case "submission_desc":
                    discussions = discussions.OrderByDescending(d => d.Timestamp);
                    break;
                default:
                    discussions = discussions.OrderByDescending(d => d.Timestamp);
                    break;
            }
            return View(discussions.ToList());
        }

        // GET: Discussions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            return View(discussion);
        }

        // GET: Discussions/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CreatorID = new SelectList(db.Users, "UserID", "Email");
            return View();
        }

        // POST: Discussions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,URL,Author,ArticleTitle,DiscussionTitle,CreatorID,Timestamp,Text")] Discussion discussion)
        {
            if (ModelState.IsValid)
            {
                db.Discussions.Add(discussion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CreatorID = new SelectList(db.Users, "UserID", "Email", discussion.CreatorID);
            return View(discussion);
        }

        // GET: Discussions/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatorID = new SelectList(db.Users, "UserID", "Email", discussion.CreatorID);
            return View(discussion);
        }

        // POST: Discussions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,URL,Author,ArticleTitle,DiscussionTitle,CreatorID,Timestamp,Text")] Discussion discussion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(discussion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatorID = new SelectList(db.Users, "UserID", "Email", discussion.CreatorID);
            return View(discussion);
        }

        // GET: Discussions/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            return View(discussion);
        }

        // POST: Discussions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Discussion discussion = db.Discussions.Find(id);
            db.Discussions.Remove(discussion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This is a POST that takes in a string (formated as JSON) and enters the information as a comment into the database.
        /// </summary>
        /// <param name="markers">String that is formated as JSON.</param>
        /// <returns>This will return a JsonResult with data to display all comments.</returns>
        [HttpPost]
        public JsonResult AddComment(string markers)
        {

            //Format string for easier access of content.
            var Data = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Dictionary<string, string>>(markers);
            

            //Make Comment object.
            Comment comment = new Comment
            {
                Timestamp = DateTime.Now,
                UserID = 1,
                CommentText = Data["comment"],
                DiscussionID = Convert.ToInt32(Data["ID"]),
                UpVote = 0,
                DownVote = 0,
                Fact = 0,
                CounterFact = 0,
                General = 0,
                OpinionBased = 0,
                Complaint = 0,
                Helpful = 0,
                Unhelpful = 0,
                Trolling = 0,
                Rude = 0,
                Inappropriate = 0
            };
            
            //Add comment to db.
            db.Comments.Add(comment);
            db.SaveChanges();


            var one = Data["comment"];

            var data = new { commentText = one, firstName = "Anonymous", lastName = "Anonymous" };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This will create a Discussion Object and return parts of it as JsonResult.
        /// </summary>
        /// <param name="id">The id of a discussion.</param>
        /// <returns>A JsonResult with data of comments.</returns>
        public JsonResult CommentRefresh(int? id)
        {

            //Make list of Comments.
            var commentsList = db.Discussions.Find(id).Comments.OrderByDescending(n => n.Timestamp);

            //Take text for comments.
            var commentTextList = commentsList.Select(n => n.CommentText).ToList();

            var commentTimeStamps = commentsList.Select(n => n.Timestamp).Select(n => n.ToString()).ToList();

            //Since all users can comment then everyone is set as Anonymous
            var data = new { commentsText = commentTextList, firstNames = "Anonymous", lastNames = "Anonymous", TimeStamps = commentTimeStamps };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
