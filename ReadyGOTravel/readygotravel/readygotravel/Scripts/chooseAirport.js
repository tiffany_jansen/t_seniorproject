﻿//This gets the info from the StartAirport DropDown.
$('#StartAirport').change(function () {
    //get the state name.
    var id = $('#StartAirport').val().toString();

    //if the user reselects the --- reset the dropdown.
    if (id == "---") {
        $('#StartAirportSpot').empty();
    }

    //the source for the json object.
    var source = "/Searches/Regions/" + id;     

    //ajax call
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: successStartAjax,
        error: errorAjax
    });
})

//This gets the info from the EndAirport DropDown.
$('#EndAirport').click(function () {
    //get the state name.
    var id = $('#EndAirport').val().toString();

    //if the user reselects the --- reset the dropdown.
    if (id == "---") {
        $('#EndAirportSpot').empty();
    }

    //the source for the json object.
    var source = "/Searches/Regions/" + id;

    //ajax call
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: successEndAjax,
        error: errorAjax
    });
})

//This is the success method for the StartAirport.
function successStartAjax(regions) {
    //parse the json object.
    var json = JSON.parse(regions);

    //if there is data in there do this stuff.
    if (json.length > 0) {        
        $('#StartAirportSpot').empty(); //clear the spot for the dropdown
        $('#StartLabel').empty(); 
        $('#StartLabel').append('<label class="control-label col-lg-12">' + "Departing Airport" + '</label>')
        $('#StartAirportSpot').append('<select name = "sAirport">'); //create the select thing and name it.
        i = 0; //starting index.
        while (i < json.length) {
            var location = getLocationString(json[i]["Location"]); //get the location string.
            if (location == null) { //if it's null don't add it to the option.
                $('#StartAirportSpot select').append('<option ' + 'value = ' + json[i]["AirportID"] + '>' + json[i]["AirportCode"] + '</option>');
            }            
            else { //if not create the options with the location.
                $('#StartAirportSpot select').append('<option ' + 'value = ' + json[i]["AirportID"] + '>' + location + " (" + json[i]["AirportCode"] + ")" + '</option>');
            }
            i++; //increment the index for the json object.
        }
        $('#StartAirportSpot select').append('</select'); //add the closing for the select.
    }
}

//This is the success method for the EndAirport.
function successEndAjax(regions) {    
    //parse the json object.
    var json = JSON.parse(regions);

    //if there is data in there do this stuff.
    if (json.length > 0) {
        $('#EndAirportSpot').empty(); //clear the spot for the dropdown
        $('#EndLabel').empty(); 
        $('#EndLabel').append('<label class="control-label col-lg-12">' + "Arriving Airport" + '</label>')
        $('#EndAirportSpot').append('<select name = "eAirport">'); //create the select thing and name it.
        i = 0; //starting index.
        while (i < json.length) {
            var location = getLocationString(json[i]["Location"]); //get the location string.
            if (location == null) { //if it's null don't add it to the option.
                $('#EndAirportSpot select').append('<option ' + 'value = ' + json[i]["AirportID"] + '>' + json[i]["AirportCode"] + '</option>');
            }
            else { //if not create the options with the location.           
                $('#EndAirportSpot select').append('<option ' + 'value = ' + json[i]["AirportID"] + '>' + location + " (" + json[i]["AirportCode"] + ")"+ '</option>');
            }
            i++;  //increment the index for the json object.
        }
        $('#EndAirportSpot select').append('</select'); //add the closing for the select.
    }
}

//Get the "long version" of the location string.
function getLocationString(abbreviation) {
    if (abbreviation == "NW") {
        return "Northwest";
    }
    if (abbreviation == "N"){
        return "North";
    }
    if (abbreviation == "NE"){
        return "Northeast";
    }
    if (abbreviation == "C"){
        return "Central";
    }
    if (abbreviation == "EC"){
        return "East-Central";
    }
    if (abbreviation == "WC"){
        return "West-Central";
    }
    if (abbreviation == "S") {
        return "South";
    }
    if (abbreviation == "SW") {
        return "Southwest";
    }
    if (abbreviation == "SE") {
        return "Southeast";
    }
    else {
        return null;
    }
}

//This is the error message.
function errorAjax() {
    console.log("An error has occured");
}