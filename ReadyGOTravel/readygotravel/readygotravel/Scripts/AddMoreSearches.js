﻿$('#submited').on('click', function () {
    addElements();
});

addElements();
function addElements() {
    //this presents the total number of searches displayed (by sets of 3)
    var TotalElements = $('#TotalElements').val().toString();
    console.log("TotalElement at start of js: " + TotalElements);
    var source = "/Searches/AddSearches/" + TotalElements;
    console.log(source);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: Done,
        error: detectedError
    });
}

function Done(data) {
    console.log("------success----");

    //setup information needed to be displayed
    var startDates = data["StartDates"]
    var endDates = data["EndDates"];
    var numTravelers = data["NumTravelers"];
    var maxAmounts = data["MaxAmounts"];
    var startAirports = data["StartAirports"];
    var endAirports = data["EndAirports"];
    var startAirportCodes = data["StartAirportCodes"];
    var endAirportCodes = data["EndAirportCodes"];
    var startAirportLocations = data["StartAirportLocations"];
    var endAirportLocations = data["EndAirportLocations"];
    var hotelStarValues = data["HotelStarValues"]

    //setup up rows for the searches table and add them
    for (i = 0; i < startDates.length; i++) {
        var TableTr = $('<tr>');

        //setting up date format for startDate
        var startDateSplit = startDates[i].split(" ");
        var col1;
        if (startDateSplit[1] == "")
            col1 = $('<td>').text(startDateSplit[0] +" "+ startDateSplit[2] +" "+ startDateSplit[3]);
        else
            col1 = $('<td>').text(startDateSplit[0] + " " + startDateSplit[1] + " " + startDateSplit[2]);

        //setting up date format for endDate
        var endDateSplit = endDates[i].split(" ");
        var col2;
        if (endDateSplit[1] == "")
            col2 = $('<td>').text(endDateSplit[0] + " " + endDateSplit[2] + " " + endDateSplit[3]);
        else
            col2 = $('<td>').text(endDateSplit[0] + " " + endDateSplit[1] + " " + endDateSplit[2]);

        var col3 = $('<td>').text(numTravelers[i]);
        var col4 = $('<td>').text("$" + maxAmounts[i]);
        var col5;
        if (startAirportLocations[i] != null)
            col5 = $('<td>').text(startAirports[i] + " " + startAirportLocations[i] + " (" + startAirportCodes[i] + ")");
        else
            col5 = $('<td>').text(startAirports[i] + " (" + startAirportCodes[i] + ")");
        var col6
        if (endAirportLocations[i] != null)
            col6 = $('<td>').text(endAirports[i] + " " + endAirportLocations[i] + " (" + endAirportCodes[i] + ")");
        else
            col6 = $('<td>').text(endAirports[i] + " (" + endAirportCodes[i] + ")");
        var col7
        if (hotelStarValues[i] == 0)
            col7 = $('<td>').text("No Preference");
        else
            col7 = $('<td>').text(hotelStarValues[i] - 1 + "-" + hotelStarValues[i]);

        TableTr.append(col1);
        TableTr.append(col2);
        TableTr.append(col3);
        TableTr.append(col4);
        TableTr.append(col5);
        TableTr.append(col6);
        TableTr.append(col7);

        //add row to table with the class "table"
        $('.table').append(TableTr);
    }

    //increment the value with TotalElements id to show table is larger
    $('#TotalElements').val(parseInt($('#TotalElements').val()) + 10);

    //decide whether to show to hide the "show more" button
    if (parseInt($('#TotalSearches').val()) > parseInt($('#TotalElements').val())) {
        $('#submited').show();
    }
    else {
        $('#submited').hide();
    }
    
}
function detectedError() {
    console.log("Error. Cause could be from AddSearches method in Searches controller. AddMoreSearches.js could also be the cause. Could also be that javascript isn't working for some other reason");
}