namespace readygotravel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class State
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public State()
        {
            Airports = new HashSet<Airport>();
        }

        public int StateID { get; set; }

        [Required]
        [Display(Name = "State Name")]
        public string StateName { get; set; }

        public int CountryID { get; set; }

        [Column(TypeName = "money")]
        [Display(Name = "Food Cost")]
        public decimal FoodCost { get; set; }

        [Display(Name = "Spring Temperature")]
        public int SpringTemp { get; set; }

        [Display(Name = "Summer Temperature")]
        public int SummerTemp { get; set; }

        [Display(Name = "Fall Temperature")]
        public int FallTemp { get; set; }

        [Display(Name = "Winter Temperature")]
        public int WinterTemp { get; set; }

        [Display(Name = "Spring Weather")]
        public string SpringWeather { get; set; }

        [Display(Name = "Summer Weather")]
        public string SummerWeather { get; set; }

        [Display(Name = "Fall Weather")]
        public string FallWeather { get; set; }

        [Display(Name = "Winter Weather")]
        public string WinterWeather { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Airport> Airports { get; set; }

        public virtual Country Country { get; set; }
    }
}
