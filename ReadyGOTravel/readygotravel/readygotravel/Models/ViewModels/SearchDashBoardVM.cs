﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace readygotravel.Models.ViewModels
{
    public class SearchDashBoardVM
    {
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MMM/dd/yyyy}")]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MMM/dd/yyyy}")]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "Number of Travelers")]
        public int NumTravelers { get; set; }

        [Required]
        [Display(Name = "Max Budget")]
        [RegularExpression(@"^\d+(?:.\d{0,2})?$", ErrorMessage = "Please enter only a positive value.")]
        public decimal MaxAmount { get; set; }

        [Display(Name = "Hotel Star Value")]
        public int? HotelStarValue { get; set; }

        [StringLength(2)]
        [Display(Name = "Flight Type")]
        public string FlightType { get; set; }
    }
}