namespace readygotravel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Search
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Search()
        {
            Results = new HashSet<Result>();
        }

        public int SearchID { get; set; }

        public int UserID { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MMM/dd/yyyy}")]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MMM/dd/yyyy}")]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "Number of Travelers")]
        public int NumTravelers { get; set; }

        [Column(TypeName = "money")]
        [Required]
        [Display(Name = "Max Budget")]
        [RegularExpression(@"^\d+(?:.\d{0,2})?$", ErrorMessage = "Please enter only a positive value.")]
        public decimal MaxAmount { get; set; }

        [Display(Name = "Hotel Star Value")]
        public int? HotelStarValue { get; set; }

        [StringLength(2)]
        [Display(Name = "Flight Type")]
        public string FlightType { get; set; }

        [Required]
        [Display(Name = "Departing from")]
        public int StartAirport { get; set; }

        [Required]
        [Display(Name = "Arriving to")]
        public int EndAirport { get; set; }

        public virtual Airport Airport { get; set; }

        public virtual Airport Airport1 { get; set; }

        public virtual Person Person { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Result> Results { get; set; }
    }
}
