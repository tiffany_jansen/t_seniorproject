﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(readygotravel.Startup))]
namespace readygotravel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
