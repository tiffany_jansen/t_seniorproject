﻿/*
	Drop Tables so we can easily empty the DB and restart.
*/

/* Drop Our Tables*/
DROP TABLE Results;
DROP TABLE Searches;
DROP TABLE People;
DROP TABLE Airports;
DROP TABLE States;
DROP TABLE Countries;

/* Drop ASP User Tables*/
DROP TABLE AspNetUserClaims;
DROP TABLE AspNetUserLogins;
DROP TABLE AspNetUserRoles;
DROP TABLE AspNetUsers;
DROP TABLE AspNetRoles;