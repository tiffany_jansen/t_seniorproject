﻿/* Create Tables:
	* ASP Tables for Users ~ so we don't have to worry about security so much
	* Our Tables Including:
		* Person Table ~ so we can connect it to the User Tables without messing up the set up.
		* Search Table ~ so we can keep track of searches
		* Results Table ~ so we can easily keep track of what the APIs returned
*/

/*ASP Tables for the Users */
/* AspNetUsers Table */
CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([UserName] ASC);

/* AspNetRoles Table */
CREATE TABLE [dbo].[AspNetRoles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([Name] ASC);

/* AspNetUserClaims Table */
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserClaims]([UserId] ASC);

/* AspNetUserLogins Table */
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserLogins]([UserId] ASC);

/* AspNetUserRoles Table */
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserRoles]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AspNetUserRoles]([RoleId] ASC);


/* Our Tables */
/* Person Table */
CREATE TABLE [dbo].[People]
(
	[PersonID] INT IDENTITY(1,1) NOT NULL,
	[UserID] NVARCHAR(128) NOT NULL,
	[FirstName] NVARCHAR(128) NOT NULL,
	[LastName] NVARCHAR(128) NOT NULL,

	CONSTRAINT [PK_dbo.People] PRIMARY KEY CLUSTERED ([PersonID] ASC),
	CONSTRAINT [FK_dbo.People] FOREIGN KEY (UserID) REFERENCES [dbo].[AspNetUsers] (Id)
);

/* Country Table*/
CREATE TABLE [dbo].[Countries]
(
	[CountryID] INT IDENTITY(1,1) NOT NULL,
	[CountryName] NVARCHAR(MAX) NOT NULL,

	CONSTRAINT [PK_dbo.Countries] PRIMARY KEY CLUSTERED ([CountryID] ASC)
)

/* U.S. State Table */
CREATE TABLE [dbo].[States]
(
	[StateID] INT IDENTITY(1,1) NOT NULL,
	[StateName] NVARCHAR(MAX) NOT NULL,
	[CountryID] INT NOT NULL,
	[FoodCost] MONEY NOT NULL,
	[SpringTemp] INT NOT NULL,
	[SummerTemp] INT NOT NULL,
	[FallTemp] INT NOT NULL,
	[WinterTemp] INT NOT NULL,
	[SpringWeather] NVARCHAR(32) NOT NULL,
	[SummerWeather] NVARCHAR(32) NOT NULL,
	[FallWeather] NVARCHAR(32) NOT NULL,
	[WinterWeather] NVARCHAR(32) NOT NULL,

	CONSTRAINT [PK_dbo.States] PRIMARY KEY CLUSTERED ([StateID] ASC),
	CONSTRAINT [FK_dbo.States] FOREIGN KEY (CountryID) REFERENCES [dbo].[Countries] (CountryID)
)

/* U.S. Airport Table */
CREATE TABLE [dbo].[Airports]
(
	[AirportID] INT IDENTITY(1,1) NOT NULL,
	[AirportCode] NVARCHAR(5) NOT NULL,
	[Location] NVARCHAR(5) NULL,
	[StateID] INT NOT NULL,

	CONSTRAINT [PK_dbo.Airports] PRIMARY KEY CLUSTERED ([AirportID] ASC),
	CONSTRAINT [FK_dbo.Airports] FOREIGN KEY (StateID) REFERENCES [dbo].[States] (StateID)
)

/* Search Table */
CREATE TABLE [dbo].[Searches]
(
	[SearchID] INT IDENTITY(1,1) NOT NULL,
	[UserID] INT NOT NULL,
	[StartDate] DATETIME NOT NULL,
	[EndDate] DATETIME NOT NULL,
	[NumTravelers] INT NOT NULL,
	[MaxAmount] MONEY NOT NULL,
	[HotelStarValue] INT,
	[FlightType] NVARCHAR(2),
	[StartAirport] INT NOT NULL,
	[EndAirport] INT NOT NULL,

	CONSTRAINT [PK_dbo.Searches] PRIMARY KEY CLUSTERED ([SearchID] ASC),
	CONSTRAINT [FK_dbo.Searches] FOREIGN KEY (UserID) REFERENCES [dbo].[People] (PersonID),
	CONSTRAINT [FK_dbo.Searches2] FOREIGN KEY (StartAirport) REFERENCES [dbo].[Airports] (AirportID),
	CONSTRAINT [FK_dbo.Searches3] FOREIGN KEY (EndAirport) REFERENCES [dbo].[Airports] (AirportID)
);

/* Result Table */
CREATE TABLE [dbo].[Results]
(
	[ResultID] INT IDENTITY(1,1) NOT NULL,
	[SearchID] INT NOT NULL,
	[AvgHotelStar] DECIMAL(3,2) NOT NULL,
	[AvgFlightAmount] MONEY NOT NULL,
	[AvgHotelAmount] MONEY NOT NULL,
	[AvgFoodCost] MONEY NOT NULL,

	CONSTRAINT [PK_dbo.Results] PRIMARY KEY CLUSTERED ([ResultID] ASC),
	CONSTRAINT [FK_dbo.Results] FOREIGN KEY (SearchID) REFERENCES [dbo].[Searches] (SearchID)
);

/*
	Seeding: 
	* 1 AspNetUser ~ to be used to make the person have an "account" associated with it
	* 1 Person ~ to be used when someone is not logged in
*/

INSERT INTO [dbo].[AspNetUsers] (Id, Email, EmailConfirmed, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName) VALUES
('0', 'user@users.com', 1, 1, 1, 1, 1, 'user');

/* Make it so I can make a random user with ID = -1 */
SET IDENTITY_INSERT [dbo].[People] ON; 

INSERT INTO [dbo].[People] (PersonID, UserID, FirstName, LastName) VALUES
(-1, '0', 'User', 'Test');

/* Turn it back off so we can only do it once. */
SET IDENTITY_INSERT [dbo].[People] OFF; 