﻿INSERT INTO [dbo].[Countries] (CountryName) VALUES
('United States');

INSERT INTO [dbo].[States] (StateName, CountryID, FoodCost, SpringTemp, SummerTemp, FallTemp, WinterTemp, SpringWeather, SummerWeather, FallWeather, WinterWeather) VALUES
('ALABAMA', 1, 34.85, 77, 91, 78, 59, 'Rainy', 'Humid', 'Cloudy', 'Rainy'), ('ALASKA', 1, 43.05, 44, 64, 41, 25, 'Snowy', 'Cloudy', 'Cloudy', 'Snowy'), ('ARIZONA', 1,39.36, 85, 104, 88, 68, 'Sunny', 'Sunny', 'Sunny', 'Sunny'),
('ARKANSAS', 1,35.26, 72, 84, 66, 43, 'Cloudy', 'Humid', 'Cloudy', 'Rainy'), ('CALIFORNIA', 1,45.92, 72, 84, 66, 43, 'Sunny', 'Sunny', 'Sunny', 'Cloudy'), ('COLORADO', 1,42.23, 62, 86, 65, 45, 'Clear skies', 'Sunny', 'Clear skies', 'Clear skies'), 
('CONNECTICUT', 1,44.28, 57, 80, 63, 39, 'Clear skies', 'Humid', 'Cloudy', 'Cloudy'), ('DELAWARE', 1,41.00, 65, 85, 68, 45, 'Cloudy', 'Humid', 'Cloudy', 'Clear skies'), ('FLORIDA', 1,40.59, 80, 91, 80, 65, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies'), 
('GEORGIA', 1,37.72, 64, 88, 73, 54, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'), ('HAWAII', 1,47.56, 83, 88, 86, 80, 'Sunny', 'Humid', 'Humid', 'Sunny'), ('IDAHO', 1,38.13, 63, 87, 64, 40, 'Cloudy', 'Sunny', 'Cloudy', 'Cloudy'),
('ILLINOIS', 1,41.00, 58, 82, 62, 34, 'Cloudy', 'Humid', 'Cloudy', 'Rainy'), ('INDIANA', 1,36.90, 62, 83, 64, 38, 'Cloudy', 'Humid', 'Cloudy', 'Cloudy'), ('IOWA', 1,36.49, 61, 84, 62, 33, 'Cloudy', 'Humid', 'Cloudy', 'Rainy'), 
('KANSAS', 1,36.49, 66, 87, 67, 42, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies'), ('KENTUCKY', 1,35.67, 65, 85, 67, 43, 'Rainy', 'Humid', 'Cloudy', 'Cloudy'), ('LOUISIANA', 1,36.90, 78, 90, 79, 63, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies'), 
('MAINE', 1,40.18, 52, 77, 57, 31, 'Cloudy', 'Clear skies', 'Cloudy', 'Cloudy'), ('MARYLAND', 1,44.69, 64, 87, 68, 44, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies'), ('MASSACHUSETTS', 1,43.46, 55, 79, 61, 38, 'Cloudy', 'Sunny', 'Clear skies', 'Clear skies'), 
('MICHIGAN', 1,38.13, 57, 80, 60, 32, 'Clear skies', 'Sunny', 'Clear skies', 'Cloudy'), ('MINNESOTA', 1,39.77, 57, 82, 58, 28, 'Cloudy', 'Rainy', 'Clear skies', 'Snowy'), ('MISSISSIPPI', 1,34.44, 76, 91, 77, 58, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies'),
('MISSOURI', 1,36.08, 66, 86, 68, 42, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'), ('MONTANA', 1,38.95, 57, 82, 58, 34, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy'), ('NEBRASKA', 1,36.90, 62, 86, 63, 35, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'),
('NEVADA', 1,40.18, 79, 101, 80, 59, 'Sunny', 'Sunny', 'Sunny', 'Sunny'), ('NEW HAMPSHIRE', 1,43.05, 56, 80, 60, 34, 'Cloudy', 'Clear skies', 'Clear skies', 'Cloudy'), ('NEW JERSEY', 1,45.92, 61, 84, 65, 41, 'Cloudy', 'Sunny', 'Clear skies', 'Clear skies'),
('NEW MEXICO', 1,38.54, 65, 84, 66, 45, 'Clear skies', 'Sunny', 'Clear skies', 'Clear skies'), ('NEW YORK', 1,46.33, 60, 82, 65, 41, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'), ('NORTH CAROLINA', 1,36.90, 71, 87, 71, 53, 'Cloudy', 'Humid', 'Cloudy', 'Rainy'),  
('NORTH DAKOTA', 1,37.72, 55, 81, 56, 25, 'Rainy', 'Clear skies', 'Cloudy', 'Snowy'), ('OHIO', 1,36.08, 62, 83, 64, 39, 'Cloudy', 'Humid', 'Cloudy', 'Snowy'), ('OKLAHOMA', 1,36.49, 71, 91, 73, 52, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies'),  
('OREGON', 1,40.59, 61, 79, 64, 48, 'Rainy', 'Sunny', 'Rainy', 'Rainy'), ('PENNSYLVANIA', 1,40.18, 63, 85, 67, 43, 'Cloudy', 'Humid', 'Clear skies', 'Rainy'), ('RHODE ISLAND', 1,40.59, 58, 80, 63, 39, 'Cloudy', 'Sunny', 'Clear skies', 'Rainy'), 
('SOUTH CAROLINA', 1,36.49, 76, 91, 76, 58, 'Cloudy', 'Humid', 'Clear skies', 'Rainy'), ('SOUTH DAKOTA', 1,35.67, 55, 81, 56, 24, 'Cloudy', 'Sunny', 'Cloudy', 'Cloudy'), ('TENNESSEE', 1,36.49, 72, 90, 74, 52, 'Cloudy', 'Humid', 'Clear skies', 'Rainy'),  
('TEXAS', 1,39.77, 79, 95, 81, 63, 'Clear skies', 'Humid', 'Sunny', 'Clear skies'), ('UTAH', 1,39.77, 61, 87, 64, 40, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy'), ('VERMONT', 1,41.82, 54, 78, 57, 30, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy'),  
('VIRGINIA', 1,41.82, 69, 88, 71, 49, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'), ('WASHINGTON', 1,43.05, 59, 74, 60, 47, 'Rainy', 'Sunny', 'Rainy', 'Rainy'), ('WEST VIRGINIA', 1,36.08, 66, 83, 67, 45, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy'),  
('WISCONSIN', 1,38.13, 53, 77, 58, 31, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy'), ('WYOMING', 1,39.36, 55, 79, 59, 39, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy');

INSERT INTO [dbo].[Airports] (AirportCode, StateID) VALUES
/* The States with only 1 International Airport */
('LIT', 4), ('DEN', 6), ('BDL', 7), ('ILG', 8), ('BOI', 12), 
('DSM', 15), ('ICT', 16), ('BWI', 20), ('BOS', 21), ('OMA', 27),
('LAS', 28), ('OKC', 36), ('PDX', 37), ('PVD', 39), ('FSD', 41),
('SLC', 44), ('BTV', 45), ('CRW', 48);

INSERT INTO [dbo].[Airports] (AirportCode, Location, StateID) VALUES
/* Alabama's International Airports */
('HSV', 'N', 1),
('BHM', 'C', 1),
/* Alaska's International Airports */
('FAI', 'N', 2), 
('ANC', 'C', 2),
('PML', 'SW', 2), ('JNU', 'S', 2), ('KTN', 'SE', 2),
/* Arizona's International Airports */
('IFP', 'NW', 3),
('PHX', 'C', 3), 
('YUM', 'SW', 3), ('TUS', 'SE', 3),
/* California's International Airports */
('SFO', 'NW', 5), ('SJC', 'N', 5), ('SMF', 'NE', 5),
('LAX', 'WC', 5), ('FAT', 'C', 5), ('SBD', 'EC', 5),
('SAN', 'SW', 5), ('ONT', 'S', 5), ('PSP', 'SE', 5), 
/* Florida's International Airports */
('PNS', 'NW', 9), ('TLH', 'N', 9), ('DAB', 'NE', 9),  
('TPA', 'WC', 9), ('MCO', 'C', 9), ('FLL', 'EC', 9),
('RSW', 'SW', 9), ('EYW', 'S', 9), ('MIA', 'SE', 9),
/* Georgia's International Airports */
('ATL', 'NW', 10), 
('SAV', 'EC', 10),
/* Hawaii's International Airports */
('HNL', 'NW', 11), 
('KOA', 'SW', 11), ('ITO', 'SE', 11),
/* Illinois's International Airports */
('MLI', 'NW', 13), ('RFD', 'N', 13), ('ORD', 'NE', 13),
('MDW', 'WC', 13), ('PIA', 'C', 13),
/* Indiana's International Airports */
('SBN', 'NW', 14), ('FWA', 'NE', 14),
('IND', 'C', 14),
/* Kentucky's International Airports */
('CVG', 'N', 17), 
('SDF', 'C', 17),
/* Louisiana's International Airports */
('AEX', 'N', 18),
('MSY', 'S', 18),
/* Maine's International Airports */
('BGR', 'EC', 19),
('PWM', 'S', 19),
/* Michigan's International Airports */
('SAW', 'NW', 22), ('MBS', 'N', 22), ('CIU', 'NE', 22),
('GRR', 'WC', 22), ('LAN', 'C', 22), ('FNT', 'EC', 22), 
('AZO', 'SW', 22), ('DTW', 'S', 22),
/* Minnesota's International Airports */
('INL', 'N', 23), ('DLH', 'NE', 23),
('MSP', 'C', 23), 
('RST', 'S', 23),
/* Mississippi's International Airports */
('JAN', 'C', 24), 
('GPT', 'S', 24),
/* Missouri's International Airports */
('MCI', 'NW', 25),
('STL', 'WC', 25),
/* Montana's International Airports */
('GPI', 'NW', 26), ('GTF', 'N', 26),
('MSO', 'WC', 26),
('BZN', 'S', 26), ('BIL', 'SE', 26), 
/* New Hampshire's International Airports */
('MHT', 'S', 29), ('PSM', 'SE', 29),
/* New Jersey's International Airports */
('EWR', 'N', 30), 
('ACY', 'S', 30),
/* New Mexico's International Airports */
('ABQ', 'NW', 31), 
('ROW', 'EC', 31),
/* New York's International Airports */
('PBG', 'NE', 32), ('ART', 'NW', 32),
('BUF', 'WC', 32), ('SYR', 'C', 32), ('ALB', 'EC', 32),
('SWF', 'S', 32), ('JFK', 'SE', 32), 
/* North Carolina's International Airports */
('GSO', 'N', 33), ('RDU', 'NE', 33),
('CLT', 'C', 33), ('ILM', 'EC', 33),
/* North Dakota's International Airports */
('ISN', 'NW', 34), ('MOT', 'N', 34), ('GFK', 'NE', 34),
('DIK', 'SW', 34), ('FAR', 'SE', 34),
/* Ohio's International Airports */
('CLE', 'N', 35),
('CMH', 'C', 35),
('DAY', 'SW', 35),
/* Pennsylvania's International Airports */
('ERI', 'NW', 38), ('AVP', 'NE', 38),
('ABE', 'EC', 38),
('PIT', 'SW', 38), ('MDT', 'S', 38), ('PHL', 'SE', 38), 
/* South Carolina's International Airports */
('GSP', 'NW', 40),
('MYR', 'EC', 40),
('CHS', 'S', 40),  
/* Tennessee's International Airports */
('BNA', 'C', 42), 
('MEM', 'SW', 42),
/* Texas's International Airports */
('LBB', 'NW', 43), ('AMA', 'N', 43), ('DFW', 'NE', 43), 
('ELP', 'WC', 43), ('MAF', 'C', 43), ('SAT', 'EC', 43),
('MFE', 'SE', 43), ('BRO', 'SW', 43),
/* Virginia's International Airports */
('IAD', 'N', 46), ('DCA', 'NE', 46),
('RIC', 'C', 46), 
('ORF', 'SE', 46),
/* Washington's International Airports */
('BLI', 'NW', 47),
('SEA', 'WC', 47), ('GEG', 'EC', 47),
/* Wisconsin's International Airports */
('GRB', 'NE', 49),
('ATW', 'EC', 49),
('MKE', 'SE', 49),
/* Wyoming's International Airports */
('JAC', 'WC', 50), ('CPR', 'C', 50);