﻿using readygotravel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using readygotravel.Controllers;
using System.Xml;
using System.Net;
using System.IO;

namespace readygotravel.Controllers
{
    public class TopLocationsController : Controller
    {
        private DBContext db = new DBContext();
        // GET: TopLocations
        public ActionResult Index()
        {
            //make an array of the top 12 states to reference
            string[] states = { "WYOMING","HAWAII", "ARIZONA", "NEW YORK", "CALIFORNIA", "MARYLAND", "MASSACHUSETTS",
                "NEVADA","ILLINOIS", "LOUISIANA", "MONTANA", "WASHINGTON"};

            //make an array of foodcost amounts and an array of avgHotel Ratings based on Searches
            decimal[] foodCostList = new decimal[12];
            string[] avgHotelList = new string[12];

            //for each of the 12 states, fill the foodcost list/AvgHotelRating List with only states with the specified names
            for (int i = 0; i < 12; i++)
            {
                //can not do a db linq query using an array, so used a work around
                string state = states[i];

                //db query to pull those states foodcosts
                foodCostList[i] = Math.Round(db.States
                    .Where(s => s.StateName.Contains(state))
                    .Select(f => f.FoodCost).FirstOrDefault(), 2);

                //try to find results for state searches
                try
                {
                    //avg the list hotel results by the selected state
                    avgHotelList[i] = Convert.ToString(Math.Round(db.States
                        .Where(s => s.StateName.Contains(state))
                        .SelectMany(a => a.Airports)
                        .SelectMany(n => n.Searches1)
                        .SelectMany(r => r.Results)
                        .Select(r => r.AvgHotelStar)
                        .Average(), 2));
                }

                //if null because of no results for that state, put in a default value
                catch
                {
                    avgHotelList[i] = "---";
                }
            }

            //ViewBags have lists for view page
            ViewBag.foodList = foodCostList;
            ViewBag.hotelList = avgHotelList;

            return View();
        }

        //Consider moving to better location later.
        private List<decimal> GetHotelData(DateTime startDate, DateTime endDate, int travelers, string location, int? star)
        {
            //Min number of rooms is 1.
            int numRooms = 1;
            //Check for "eveness"
            if (travelers % 2 == 0)
            {
                //Then just divide by 2.
                numRooms = travelers / 2;
            }
            else
            {
                //Else, add 1 then divide.
                numRooms = (travelers + 1) / 2;
            }

            //Get the API Key fromt the AppSecrets File.
            string hotelKey = System.Web.Configuration.WebConfigurationManager.AppSettings["hotelKey"];

            //Construct the Url for the hotel API
            string hotelURL = "http://api.hotwire.com/v1/search/hotel?apikey=" + hotelKey + "&dest=" + location + "&rooms=" + numRooms + "&adults=" + travelers + "&children=0&startdate=" + startDate.Month.ToString("00") + "/" + startDate.Day.ToString("00") + "/" + startDate.Year + "&enddate=" + endDate.Month.ToString("00") + "/" + endDate.Day.ToString("00") + "/" + startDate.Year;

            //Get the response string from the site so I can start accessing it.
            string responseString = GetResponseString(hotelURL);

            //Turn the string into XML Format.
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseString);

            //Get all the elements by the "total price" tag.
            XmlNodeList priceList = doc.GetElementsByTagName("TotalPrice");
            XmlNodeList starList = doc.GetElementsByTagName("StarRating");

            //Make things easy to access.
            decimal totalPrice = 0;
            decimal totalStar = 0;
            List<decimal> retList = new List<decimal>();

            //Check if star value passed in is null or 0.
            if (star == null | star == 0)
            {
                //Average out all the prices and star ratings and return that average.                
                if (priceList.Count == 0)
                {
                    retList.Add(0);
                    return retList;
                }
                for (int i = 0; i < priceList.Count; i++)
                {
                    decimal price = Convert.ToDecimal(priceList.Item(i).InnerXml);
                    decimal starValue = Convert.ToDecimal(starList.Item(i).InnerXml);
                    totalPrice += price;
                    totalStar += starValue;
                }
                //Round price to 2 decimal places so it's actually a price.
                decimal hotelPrice = Math.Round((totalPrice / priceList.Count), 2);
                retList.Add(hotelPrice);

                //Round starValue to 2 decimal places so it's cleaner.
                decimal StarAvg = Math.Round((totalStar / starList.Count), 2);
                retList.Add(StarAvg);

                return retList;
            }
            //Check the starList to the starvalue passed in.
            else
            {
                //counter
                int j = 0;

                for (int i = 0; i < starList.Count; i++)
                {
                    decimal starValue = Convert.ToDecimal(starList.Item(i).InnerXml);
                    if (starValue >= star - 1 && starValue <= star)
                    {
                        decimal price = Convert.ToDecimal(priceList.Item(i).InnerXml);
                        decimal stars = Convert.ToDecimal(starList.Item(i).InnerXml);
                        totalPrice += price;
                        totalStar += stars;
                        j++;
                    }
                }
                if (j == 0)
                {
                    retList.Add(0);
                    return retList;
                }
                //Round price to 2 decimal places so it's actually a price.
                decimal hotelPrice = Math.Round((totalPrice / j), 2);
                retList.Add(hotelPrice);

                //Round starValue to 2 decimal places so it's cleaner.
                decimal StarAvg = Math.Round((totalStar / j), 2);
                retList.Add(StarAvg);

                //return the list
                return retList;
            }
        }

        private string GetResponseString(string url)
        {
            //Make a request using my urlInfo, then grab response information 
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream information = response.GetResponseStream();
            StreamReader reader = new StreamReader(information);

            //Grab full response information, read to the end of the data and put it into a string
            return reader.ReadToEnd();
        }

        /// <summary>
        /// This involves getting the hotel cost by star rating of a specified state.
        /// </summary>
        /// <param name="id">This represents the number id associated with the number of top locations.</param>
        /// <returns>A JsonResult containing the hotel cost by star rating and the location ID that is is associated with.</returns>
        public JsonResult CostForHotelStars(int? id)
        {
            int check = id ?? default(int);

            //These are the airports codes associated with the states in the top locations page
            //The states in order are: "WYOMING","HAWAII", "ARIZONA", "NEW YORK", "CALIFORNIA", "MARYLAND", "MASSACHUSETTS","NEVADA","ILLINOIS", "LOUISIANA", "MONTANA", "WASHINGTON"}
            string[] AirportCode = { "JAC","HNL", "PHX", "JFK", "LAX", "BWI", "BOS",
                "LAS","ORD", "MSY", "BZN", "SEA"};

            DateTime date1 = DateTime.Now.AddDays(1);
            DateTime date2 = date1.AddDays(7);

            List<decimal> costsByStar = new List<decimal>();

            //Run for every star value.
            for (int i = 1; i < 6; i++)
            {
                List<decimal> hotelstuff = GetHotelData(date1, date2, 1, AirportCode[check - 1], i);
                costsByStar.Add(hotelstuff.First());
            }
            
            var data = new { locationID = check, CostsByStar = costsByStar};
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}