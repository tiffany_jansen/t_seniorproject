﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using readygotravel.Models;

namespace readygotravel.Controllers
{
    public class QuestionnaireController : Controller
    {
        private DBContext db = new DBContext();

        // GET: Questionnaire
        [HttpGet]
        public ActionResult Index()
        {
            List<State> result = new List<State>();
            string season = Request.QueryString["season"];
            string weather = Request.QueryString["weather"];
            string temperature = Request.QueryString["temperature"];

            System.Diagnostics.Debug.WriteLine(season);
            System.Diagnostics.Debug.WriteLine(weather);
            System.Diagnostics.Debug.WriteLine(temperature);

            if (temperature != null)
            {
                string[] tempSplit = temperature.Split('-');
                int[] tempInts = Array.ConvertAll(tempSplit, s => int.Parse(s));
                int temp1 = tempInts[0];
                int temp2 = tempInts[1];

                System.Diagnostics.Debug.WriteLine(tempInts[0]);
                System.Diagnostics.Debug.WriteLine(tempInts[1]);

                switch (season)
                {
                    case "spring":
                        if (weather != "no preference")
                        {
                            result = db.States.Where(s => s.SpringWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
                        }
                        else
                        {
                            result = db.States.Where(s => s.SpringWeather == "sunny" || s.SpringWeather == "humid" || s.SpringWeather == "clear skies" || s.SpringWeather == "cloudy" || s.SpringWeather == "rainy" || s.SpringWeather == "snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
                        }
                        ViewBag.Result = "springlist";
                        break;
                    case "summer":
                        if (weather != "no preference")
                        {
                            result = db.States.Where(s => s.SummerWeather == weather).Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
                        }
                        else
                        {
                            result = db.States.Where(s => s.SummerWeather == "sunny" || s.SummerWeather == "humid" || s.SummerWeather == "clear skies" || s.SummerWeather == "cloudy" || s.SummerWeather == "rainy" || s.SummerWeather == "snowy").Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
                        }
                        ViewBag.Result = "summerlist";
                        break;
                    case "fall":
                        if (weather != "no preference")
                        {
                            result = db.States.Where(s => s.FallWeather == weather).Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
                        }
                        else
                        {
                            result = db.States.Where(s => s.FallWeather == "sunny" || s.FallWeather == "humid" || s.FallWeather == "clear skies" || s.FallWeather == "cloudy" || s.FallWeather == "rainy" || s.FallWeather == "snowy").Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
                        }
                        ViewBag.Result = "fallresult";
                        break;
                    case "winter":
                        if (weather != "no preference")
                        {
                            result = db.States.Where(s => s.WinterWeather == weather).Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
                        }
                        else
                        {
                            result = db.States.Where(s => s.WinterWeather == "sunny" || s.WinterWeather == "humid" || s.WinterWeather == "clear skies" || s.WinterWeather == "cloudy" || s.WinterWeather == "rainy" || s.WinterWeather == "snowy").Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
                        }
                        ViewBag.Result = "winterresult";
                        break;
                    case "no preference":
                        //no preference
                        if (weather != "no preference")
                        {
                            result = db.States.Where(s => s.SpringWeather == weather || s.SummerWeather == weather || s.FallWeather == weather || s.WinterWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
                        }
                        else
                        {
                            result = db.States.Where(s => s.SpringWeather == "sunny" || s.SpringWeather == "humid" || s.SpringWeather == "clear skies" || s.SpringWeather == "cloudy" || s.SpringWeather == "rainy" || s.SpringWeather == "snowy" || s.SummerWeather == "sunny" || s.SummerWeather == "humid" || s.SummerWeather == "clear skies" || s.SummerWeather == "cloudy" || s.SummerWeather == "rainy" || s.SummerWeather == "snowy" || s.FallWeather == "sunny" || s.FallWeather == "humid" || s.FallWeather == "clear skies" || s.FallWeather == "cloudy" || s.FallWeather == "rainy" || s.FallWeather == "snowy" || s.WinterWeather == "sunny" || s.WinterWeather == "humid" || s.WinterWeather == "clear skies" || s.WinterWeather == "cloudy" || s.WinterWeather == "rainy" || s.WinterWeather == "snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
                        }
                        ViewBag.Result = "noprefresult";
                        break;
                    default:
                        break;
                }
                if(result.Count() == 0)
                {
                    ViewBag.Message = "I'm sorry, but there are no results for your given selections of " + season + " season, with " + weather + " weather, and " + temp1 + " to " + temp2 + " degree temperature:";
                }
                else
                {
                    ViewBag.Message = "Here are the results for your selections of " + season + " season, with " + weather + " weather, and " + temp1 + " to " + temp2 + " degree temperature:";
                }
            }
            return View(result);
        }

        public ActionResult Result()
        {
            return View();
        }
    }
}