Group Project Inception Phase Part 2
====================================

##Initial Vision

A one-stop shop for travel for those without a good concept of cost and need assistance determining where and how they can travel under a specific budget. It would allow users to find an estimate of how much it would cost to travel to a specific country and eventually certain popular destinations. Our starting point will be to focus on flights and lodging with additional concepts down the line such as: food, transportation, events, weather, crime in the area, etc. 

##Vision Statement

For users who are interested traveling, but are unsure as to if they have to budget to visit any specific location.  The "ReadyGo-Travel” is a site of convenience allowing users to quickly find out what the costs of travel would be to a destination of their choosing.  The site will allow users to provide basic information such as: how much they want to spend, the number of guests flying, their requested dates of travel, and a generalize location. The site will then provide users with an amount or range of costs that would be needed in order to accommodate flights, as well as lodging. Additional information such as food, transportation, and weather will also be available as the site continues to evolve.  Users will also be able to set up accounts and store previous searches as to quickly compare multiple travel locations and the amounts associated. Unlike other travel sites, our goal is focused around two things: one is to quickly provide basic cost information allowing for users to plan destinations based on a fixed budget, and the other is to provide a generalized daily cost amount for suggested popular destinations. The idea will be that users can quickly gain a funding foothold right out of the gate without having to select, research, and budget each component of travel one by one.  The second is to provide suggested and popular locations of travel, while quickly showing a average daily amount that would be necessary to stay in said location. Our goal is to help users get information quickly, while allowing them to save and review searches so that they can travel when it is the most affordable to them.

##Summary of our Approach to Software Development
We will be following the agile principles as described in the Disciplined agile Delivery textbook. As well as following the scrum project management iterative process. 

##Initial Requirements Elaboration and Elicitation

**Questions:**

1. What makes this project different from sites like TripAdvisor, Expedia, etc.?
    * This project will not allow you to book a hotel room or flight, but instead allows the user to input a budget, number of people, time of year, etc and returns a list of places the user could travel to. It will include pictures and information about the places like TripAdvisor does, but it’s more for figuring out where you CAN go on a specific budget.
    * We also intend to give suggested “hot spot” locations based on places that are popular to travel to. We can include additional information on these places to give people a better idea of where they should vacation.
2. Will we account for the use of different currencies?
    * We can consider this in the future once we get the basics done.
3. Will we make the site available in multiple languages?
    * We can implement this in the future once we get the basics done.
4. We need accounts and logins. Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or …?
    * No, but maybe we can implement that later?
5. What is it important to know about our users? What data should we collect?
    * Search History, Basic Login info, Name?, Location, Email, etc?
6. How do we manage inappropriate names to maintain a civil environment for users?
    * Unsure at the moment, it’s something we can look into the future.
7. What are we asking of the user?
    * Travel Dates (Time of Year?), Budget Constraints, Number of People Traveling, etc.
8. Should we include a “contact us” page so users can give feedback? If so, how are we going to make sure the feedback isn’t super rude?
9.  How much information from the interviews do we actually want to include in our site?
    * Probably not, but it will definitely help us with deciding what we want to do.
10. How will you narrow search results after a search?
11. How will we tackle the popular destinations page?
    * AJAX, or store data in database?
12. Are we going to get all of our data from APIs or are we gonna scrape some data off of other sites?

**Interviews:**

_We intended to ask interviewees these questions:_

1. What other information would you like to know about places you might visit?
    * Museums, cultural centers, famous people, and historical references.
    * Parks, Famous beaches, Local markets, well known places to eat, bed breakfasts, best places to shop, amusement park, arcades, things for kids to do, things for only adults--like bars.
    * Nearest bathroom, high traffic areas, local restaurants, aquarium (if applicable), zoos (if applicable), and cost of events.
    * Handicap accessibility, nearest medical care, nearest police station, typical hours of night life, national landmarks, public transportation and how to get bus tickets, churches/synagogues/temples/mosques for religious travelers, cleanliness, any unexpected laws/things to expect. Any special events occurring at date you are traveling   
    * Different activities to do in the surrounding area. Popular restaurants, tourist destinations, activity centers, that kind of thing.
2. What type of interface would you envision for something like this?
    * Simple, everything in one place
    * Select what you want to know, so you aren’t overwhelmed by everything. Enter budget at start so you aren’t shown inaccessible options
    * Enter a location and price point for travel and hotels. Once these are selected recommended activities could come up.
3. What do you like about other travel sites?
    * Pictures, have multiple price options for what you want.
    * Ability to compare, tells me if flights include carry-on and luggage cost
4. What do you dislike about other travel sites?
    * I dislike that not all information is not in one place
    * Overwhelmed with irrelevant material, don’t allow you to say you don’t care about specific things. 
5. If you have traveled before, what things do you wish you knew before going there?
    * The average flight time from point A to point B.
    * How to say bathroom and how much and help/police in language, phone number or how to contact emergency services. Specific laws that will affect you, rated value for money.
    * Flight time, activities, and good restaurants.

##List of Needs and Features

**_This is subject to change and not complete_**

* A great looking home page with an easy-to-use interface where users can easily find the information they are looking for.
    * This will include a form for inputting their data to see where they can travel on a specific budget or at least a link to where to find it. 
* The ability to create a user account to save destinations to their favorites, and/or save search history so they don’t have to re-input their data in order to get the results they want.
* A navbar that has links to all the necessary pages so people can easily access the site.
* A “Popular” page so people can see where people tend to travel a lot.
* Additional Info and pages for each of the popular places.
* Results page after user input.
* Profile Page where user can save their data
    * Maybe some pictures of places that are similar in budget/other stuff that they have searched for previously.
    * Search History and/or Favorites

##Initial Modeling

**Use Case Diagram:**

![Image](caseDiagram.png)

**Other Data Modeling:**

![Image](map.png)

##Identify Non-Functional Requirements

1. User accounts and data must be stored indefinitely.
2. All queries should be tracked and logged so that user history, general traffic, and data can be analyzed and used for site improvements/modifications.
3. Query Results should also be tracked to identify changes in costs for trending and additional data that could be compiled to help users see changed in the cost of travel.
4. The ability to access the APIs and fill the database(s) with information so we can do the budget information and stuff.
5. We need to be able to create an algorithm that will take the input given and return a list with all the possibilities for the user.
6. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
7. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
8. Site should be able to handle at least 500000 users at a time without crashing. 
9. A contact us section which may include Site administrators who will be in charge of looking into potential abuse or concerns from other users.
10. Site should automatically send a confirmation email once a user has signed up.
11. Add a .gitignore to make sure we don’t end up with a bunch of extra stuff in our Repo.
12. Use ASP.Net Identity for the user accounts so users cannot be hacked so easily.

##Identify Functional Requirements (User Stories)

1. [E] Homepage with functionality for users and non-users.
    * [F] The ability to enter in specific form information and get back a generalize suggested budget value
        * [U] As a user, I would like to be able to enter in my desired location, budget, number of people traveling, and dates of travel so that I can get a general idea of how much it will cost me to take a trip.
        * [U] As a user, I would like to get back either a list of options based on my search criteria or a general block of data that matches my search so that I know whether or not I can travel based on the information I put into your site.
        * [U] As a user, I would like to be able to input only the information I want to (like just budget and number of people) and still get search results that relate to the information, so that I don’t have to know the answer to everything to get possible vacation destinations.
    * [F] The Homepage should have a clean design and layout
        * [U] As a user, I would like to have an clean landing page that is easy to navigate so that I fully understand what I am looking at and how to utilize your site as to get the most out of it.
    * [F] Navigation Bar for easy navigation including registering and logging in links.
        * [U] As a user, I would like to be able to login or register with your site so that I can review a general history of previous searches that I have made previously.
    * [F] Footer with more information about the site such as about us and contact us pages.
        * [U] As a user, I would like a place to go to find out more information about your site so that I can contact you in the case of missing or erroneous information.
        * [U] As a user, I would like to have convenient links on the navigation bar to let me search for just flights, just hotels, flights/hotels, as well as links to popular locations or events.
        * [U] As a logged in user, I would like to easily find a place to submit pictures from my vacation so they can be used on the site to get others to go.
    * [F] Basic Information about the site
        * [U] As a user, I would like to see some of the basic information about the site, so I can decide if I actually want to use it or not.
        * [U] As an admin, I would like to be able to easily change the home page so it correlates to the time of year we are in right now.
2. [E] Top Locations Page that includes generic travel information about some of the most traveled locations.
    * [F] Should include Pictures of the top x locations and generic travel information such as price per day on average per person (with details as to what this price generally includes).
        * [U] As a user, I would like to have an area that shows some of the most popular vacation destinations, as well as a general cost, so that I have something that helps me to find a cool place to take a trip.
        * [U] As a user, I would like to be able to click on some of these popular destinations to get more information about these areas such as general food, weather, crime, etc..so that I can better decide on a location that I would like to visit.
3. [E] Each Top Location should have or link to their own unique homepage.
    * [F] Each page should include additional information about each of the top locations such as weather, crime, food, etc…
        * [U] As a user, I would like to see what food options are available so that I can better determine if this would be a location that I would like to travel.
        * [U] As a user, I would like to know what the weather would be like in one of these top locations so I can decide if this is a location I would like to potentially travel based on the outside conditions during my stay.
        * [U] As a user, I would like to know the crime rate of the top locations, so I can decide if the location is safe enough for me and my travel companions.
4. [E] Fully Functional User Accounts
    * [F] The ability to create a user account
        * [U] As a user, I would like to be able to create an account so I can save searches to my favorites.
        * [U] As a user, I would like to be able to change and update my basic information such as address, email, name, or password.
        * [U] As a user, I would like to be able to easily share my saved trip ideas to social media websites like Facebook, Instagram, or Twitter.
        * [U] As a user, I would like to be able to Login after I have made an account, so I can look at my previous search history/Favorites.(might be able to change and break up into two)
        * [U] As a user, I would like to be able to delete saved favorite searches on my account so that I can remove the things I don't care about anymore.
        * [U] As a user I would like to be able to see a few places side by side so I can easily choose which one I would rather go to.
    * [F] Keep passwords protected so users feel safe
        * [U] As a user, I would like to know what the site is doing with my information so I can feel confident the site won’t be selling my information.
        * [U] As a user, I would like my password hidden when I am entering it to login in so that people looking at my screen can’t see my password.
5. [E] Search Results Page
    * [F] The Page
        * [U] As a user I would like a clean results page, so that I can easily choose which destination I would like to go to.
            * [T] Save results to the results table with the search id as a foreign key.
        * [U] As a user, I would like to be able to easily share my search/trip ideas to social media websites like Facebook, Instagram, or Twitter 
        * [U] As a user, I would like all the relevant information on the search results page of each of the destinations that I am able to travel to, so I can easily choose which one I would rather go to.
        * [U] As a user, I would like a link to sites where I can book flights and/or hotels so I can book my flight/hotel to the location I want to go.
        * [U] As a user I would like my search results to be in pages (not a long list of results) so that it can be more organized and I can jump between pages instead of dealing with a long page with many results to look back on.
            * [T] Figure out how to get everything onto pages instead of one long list.
        * [U] As a user I would only like to be able to find more information about a place I want to visit, so I can choose where I would like to go.
            * [T] Set up links with the designated location with information about activities, food in the area, etc.
        * [U] As a logged in user I would like to be able to save specific destinations so I can easily find them again.
        * [U] As a user, I would like to be able to select at least 2 destinations to see the details side-by-side so I can easily choose between them.
        * [U] As an admin, I would like a place to see what destinations are saved the most so I can potentially add that destination to the Top Locations page.
        * [U] As a user, I would like similar destinations to show up on a side panel so I can see if any of those interest me enough to go there.
6. [E] Admin Pages
       * [U] As an admin, I would like a place where I can easily change the background of the website so I can keep it up to date with the current season.
       * [U] As an admin, I would like to be able to easily add a location to the “Top Location Page” without knowing a lot about programming so I can keep the site up to date.


##Initial Architecture Envisioning

* SQL Server (Database Storage)
* Azure for deployment
* ASP.NET/Visual Studio
* Client/Server Functionality
* Network
* MVC is going to be our software design
* ASP.Net Identity~For User Accounts
* Scrape Data?
* API’s to be used.
    * TravelPayouts ~ For Flights
    * Uber ~ For Mileage
    * Refer to other Document
        * Still trying to find a good one for hotels, the first one on the other document does include prices so worst comes to worst we can use that

##Initial Agile Data Modeling

**_This is subject to change and not complete_**

![Image](ERD.png)

##Time and Release Plan

**Inception Period:** Jan 25th to Feb 10th

Sprint 1: Feb 11th to Feb 24th -> **First Release:** February 24th

Sprint 2: Feb 25th to March 10th -> **Second Release:** March 10th

Sprint 3: April 1st to April 14th -> **Third Release:** April 14th

Sprint 4: April 15th to April 28th -> **Fourth Release:** April 28th

Sprint 5: April 29th to May 12th -> **Fifth Release:** May 12th 

Sprint 6: May 13th to May 26th - > **Sixth Release:** May 26th

**Testing Period:** TBA

**_Final Release:_** May 30th